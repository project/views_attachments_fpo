<?php


/**
 * @file
 * Contains the fpo attachment display plugin.
 */

/**
 * The plugin that handles an attachment display, which alterations for FPO.
 *
 * We add additional functionality to the default attachment, cheifly we can
 * detect if we are on the first page, and we can hide results in our parent
 * views (that we've probably already shown).
 *
 * @see views_plugin_display_attachment
 * @ingroup views_display_plugins
 */
class views_attachment_fpo_plugin_display_attachment  extends views_plugin_display_attachment {

  function option_definition () {
    $options = parent::option_definition();

    $options['show_only_on_first_page'] = array('default' => FALSE);
    $options['hide_main_results'] = array('default' => FALSE);

    return $options;
  }

  function execute() {
    $content = $this->view->render($this->display->id);
    // Only show the rendered view on the first page:
    if ($this->get_option('show_only_on_first_page') && !empty($this->view->pager['current_page']) && $this->view->pager['current_page'] != 0) {
      // Remove the results too:
      foreach ($this->view->result as $k => $v) {
        unset($this->view->result[$k]);
      }
      return;
    }
    else {

      return $content;
    }
  }

  /**
   * Provide the summary for attachment options in the views UI.
   *
   * This output is returned as an array.
   */
  function options_summary(&$categories, &$options) {
    // It is very important to call the parent function here:
    parent::options_summary($categories, $options);

    $options['attachment_position']['value'] .= $this->get_option('show_only_on_first_page') ? ' (only on first page)' : '';

    $options['hide_main_results'] = array(
      'category' => 'attachment',
      'title' => t('Hide main view results'),
      'value' => $this->get_option('hide_main_results') ? t('Yes') : t('No'),
    );

  }

  /**
   * Provide the default form for setting options.
   */
  function options_form(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_form($form, $form_state);

    switch ($form_state['section']) {
      case 'attachment_position':
        $form['show_only_on_first_page'] = array(
          '#type' => 'checkbox',
          '#title' => t('Show only on the first page'),
          '#description' => t('Tick this to only show this attachment on the first page. This will also remove the number of results shown by the attached view from the main view.'),
          '#default_value' => $this->get_option('show_only_on_first_page'),
        );
        break;
      case 'hide_main_results':
        $form['hide_main_results'] = array(
          '#type' => 'checkbox',
          '#title' => t('Hide results in main view'),
          '#description' => t('This will remove N results from the view this attachment is attached to when it is viewed. N is the items per page value.'),
          '#default_value' => $this->get_option('hide_main_results'),
        );
        break;
    }
  }

  /**
   * Perform any necessary changes to the form values prior to storage.
   * There is no need for this function to actually store the data.
   */
  function options_submit(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_submit($form, $form_state);
    switch ($form_state['section']) {
      case 'attachment_position':
        $this->set_option('show_only_on_first_page', $form_state['values']['show_only_on_first_page']);
        break;
      case 'hide_main_results':
        $this->set_option($form_state['section'], $form_state['values'][$form_state['section']]);
        break;
    }
  }

  /**
   * Attach to another view.
   */
  function attach_to($display_id) {
    $displays = $this->get_option('displays');

    if (empty($displays[$display_id])) {
      return;
    }

    if (!$this->access()) {
      return;
    }

    // Get a fresh view because our current one has a lot of stuff on it because it's
    // already been executed.
    $view = $this->view->clone_view();
    $view->original_args = $view->args;

    $args = $this->get_option('inherit_arguments') ? $this->view->args : array();
    $view->set_arguments($args);
    $view->set_display($this->display->id);
    if ($this->get_option('inherit_pager')) {
      $view->display_handler->use_pager = $this->view->display[$display_id]->handler->use_pager();
      $view->display_handler->set_option('pager_element', $this->view->display[$display_id]->handler->get_option('pager_element'));
    }

    // because of this, it is very very important that displays that can accept
    // attachments not also be attachments, or this could explode messily.
    $attachment = $view->execute_display($this->display->id, $args);

    // Do the special result removing stuff:
    if ($this->get_option('hide_main_results')) {
      $result_count = count($view->result);

      if (!isset($this->view->build_info['fpo_remove_before'])) {
        $this->view->build_info['fpo_remove_before'] = 0;
      }
      if (!isset($this->view->build_info['fpo_remove_after'])) {
        $this->view->build_info['fpo_remove_after'] = 0;
      }

      switch ($this->get_option('attachment_position')) {
        case 'before':
          $this->view->build_info['fpo_remove_before'] += $result_count;
          break;
        case 'after':
          $this->view->build_info['fpo_remove_after'] += $result_count;
          break;
        case 'both':
          $this->view->build_info['fpo_remove_before'] += $result_count;
          $this->view->build_info['fpo_remove_after'] += $result_count;
          break;
      }
    }

    switch ($this->get_option('attachment_position')) {
      case 'before':
        $this->view->attachment_before .= $attachment;
        break;
      case 'after':
        $this->view->attachment_after .= $attachment;
        break;
      case 'both':
        $this->view->attachment_before .= $attachment;
        $this->view->attachment_after .= $attachment;
        break;
    }

    $view->destroy();
  }
}
