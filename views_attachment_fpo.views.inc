<?php


/**
 * Implementation of hook_views_plugins_alter().
 *
 * Changes the attachment handler that comes with views to use our handler,
 * which hopefully won't break anything, but I wouldn't be surprised.
 */
function views_attachment_fpo_views_plugins_alter(&$plugins) {

  // Let's make sure no one else has been altering before us:
  if ($plugins['display']['attachment']['handler'] == 'views_plugin_display_attachment') {

    // Now we can alter in out plugin:
    $plugins['display']['attachment_base'] = array(
      'handler' => $plugins['display']['attachment']['handler'],
      'no ui' => TRUE,
      'path' => $plugins['display']['attachment']['path'],
      'file' => $plugins['display']['attachment']['file'],
      'parent' => 'parent',
    );
    $plugins['display']['attachment']['handler'] = 'views_attachment_fpo_plugin_display_attachment';
    $plugins['display']['attachment']['path'] = drupal_get_path('module', 'views_attachment_fpo');
    $plugins['display']['attachment']['file'] = 'views_attachment_fpo_plugin_display_attachment.inc';
    $plugins['display']['attachment']['parent'] = 'attachment_base';
  }

}
